package bo;

import driver.DriverManager;
import po.GmailLoginPO;

public class LoginBO {
    GmailLoginPO gmailLoginPO;

    public LoginBO() {
        this.gmailLoginPO = new GmailLoginPO(DriverManager.getDriver());
    }

    public void loginUser(String login, String password) {
        gmailLoginPO.inputLoginAndClick(login);
        gmailLoginPO.inputPasswordAndClick(password);
    }
}
