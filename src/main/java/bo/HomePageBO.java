package bo;

import driver.DriverManager;
import model.Message;
import po.AlertPO;
import po.GmailHomePagePO;

public class HomePageBO {
    private GmailHomePagePO gmailHomePagePO;
    private AlertPO alertPO;

    public HomePageBO() {
        this.gmailHomePagePO = new GmailHomePagePO(DriverManager.getDriver());
        this.alertPO = new AlertPO(DriverManager.getDriver());
    }

    public void sendMessageWithInvalidEmail(Message message) {
        gmailHomePagePO.clickOnWriteAMessageButton();
        gmailHomePagePO.writeAMessageTo(message.getInvalidEmail());
        gmailHomePagePO.writeASubjectOfMessage(message.getSubject());
        gmailHomePagePO.writeAMessage(message.getMessage());
        gmailHomePagePO.sendAMessage();
    }

    public void sendMessageWithValidEmail(Message message) {
        gmailHomePagePO.clickOnWriteAMessageButton();
        gmailHomePagePO.writeAMessageTo(message.getReceiver());
        gmailHomePagePO.writeASubjectOfMessage(message.getSubject());
        gmailHomePagePO.writeAMessage(message.getMessage());
        gmailHomePagePO.sendAMessage();
    }


    public boolean verifyAlertIsPresent(String invalidEmail) {
        return alertPO.getAlertMessage().contains(invalidEmail);
    }

    public void clickOnCloseAlertAndMessage() {
        alertPO.clickOkOnAlertPopup();
        gmailHomePagePO.clickAndCloseMessage();
    }

    public boolean verifyMessageSentSuccessfully(String message) {
        gmailHomePagePO.openSentMessage();
        return gmailHomePagePO.getMessageDetails().contains(message);
    }
}
