package po;

import decorator.Button;
import decorator.Label;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

public class AlertPO extends BasePage {

    public AlertPO(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//*[@role='alertdialog']/div[2]")
    private Label alertMessage;

    @FindBy(xpath = "//button[@name='ok']")
    private Button submitAlertPopup;

    public String getAlertMessage() {
        waitElementToBeClickable(alertMessage);
        return alertMessage.getText();
    }

    public void clickOkOnAlertPopup() {
        waitElementToBeClickable(submitAlertPopup);
        submitAlertPopup.click();
    }
}
