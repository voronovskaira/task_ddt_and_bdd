import bo.HomePageBO;
import bo.LoginBO;
import driver.DriverManager;
import model.Message;
import model.Users;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import util.JAXBParser;
import util.OpenCSVParser;
import util.PropertiesReader;

public class GmailTest {
    private Message message = JAXBParser.getMessageModelFromXML(PropertiesReader.getInstance().getMessageXMLPath());
    private Users users = OpenCSVParser.getUsersFromCSV(PropertiesReader.getInstance().getUsersCSVPath());


    @BeforeMethod
    public void setUp() {
        DriverManager.getDriver().get(PropertiesReader.getInstance().getGmailURL());
    }

    @DataProvider(parallel = true)
    public Object[][] users() {
        Object[][] objects = new Object[users.getUsersList().size()][2];
        for (int i = 0; i < users.getUsersList().size(); i++) {
            objects[i][0] = users.getUsersList().get(i).getEmail();
            objects[i][1] = users.getUsersList().get(i).getPassword();
        }
        return objects;
    }

    @Test(dataProvider = "users")
    public void testGmail(String userEmail, String password) {
        LoginBO loginBO = new LoginBO();
        loginBO.loginUser(userEmail, password);
        HomePageBO homePageBO = new HomePageBO();
        homePageBO.sendMessageWithInvalidEmail(message);
        Assert.assertTrue(homePageBO.verifyAlertIsPresent(message.getInvalidEmail()));
        homePageBO.clickOnCloseAlertAndMessage();
        homePageBO.sendMessageWithValidEmail(message);
        Assert.assertTrue(homePageBO.verifyMessageSentSuccessfully(message.getMessage()));
    }

    @AfterMethod
    public void tearDown() {
        DriverManager.quit();
    }
}

